CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Dependencies
 * Installation
 * Structure
 * Similar modules


INTRODUCTION
------------

This module adds the ability to sell a copy of an existing node as a product.
By selling a copy, it provides the ability to sell nodes in a very easy and 
lightweight manner, with minimal configurations.

You may:
- Use as many node reference fields you'd like
- Sell multiple nodes using a single reference field.

DEPENDENCIES
------------

Commerce (and its dependencies)
References (node_reference)

INSTALLATION
------------

- Enable this module.
- Create a node-reference field on your-product-type.
- Goto the field preferences of the field you just created.
- Check the Nodecopy checkbox.
- Create a node in the way you want it to show to the buyer when purchased.
- Create a new product of your-product-type
- Set node reference field to the newly created node

Wait till someone buys something, and profit!

- Optional: Duplicate your field on the line item (new node ID writeback).


STRUCTURE
---------

- A checkbox field on each node reference field configuration (instances on products only).
- A default rule that copies the node
- A writeback rule that writes the node's ID back to the line item object.

Be aware that it copies the exact node (with publish state & everything), 
so you will in most cases need to publish the node by adding to the rule.

USE-CASE
--------

- You sell ads configured by the user after purchase with restricted fields.
- You want to start selling nodes in 1 minute.
- You have 10 different products that each sell a variant of the same
  node type. Instead of spending time configuring rules, you want to have 10 
  dummy nodes. That way you can easily reconfigure each product.

SIMILAR MODULES
---------------

This module is somewhat similar to Commerce Node Checkout, 
but is a bit more lightweight and has a different approach.

While CNC creates the nodes first, then checks out, this module lets the admin
create a default node for the product. This means that you can use restricted
fields on the node to control its behaviour in your system.